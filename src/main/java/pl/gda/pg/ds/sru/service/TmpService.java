package pl.gda.pg.ds.sru.service;

import pl.gda.pg.ds.sru.entity.*;
import pl.gda.pg.ds.sru.entity.helper.BPassword;
import pl.gda.pg.ds.sru.entity.helper.UserType;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

@Stateless
public class TmpService {
    @PersistenceContext
    private EntityManager entityManager;

    @GET
    @Path("/create")
    public Object createAdmin(@Context SecurityContext securityContext) {
        SruUser sruUser = new SruUser();
        sruUser.setFirstName("Adminowski");
        sruUser.setLastName("Użytkownik");
        sruUser.setUserType(UserType.ADMINISTRATOR);
        sruUser.setLogin("adminn");
        sruUser.setbPassword(new BPassword("123456"));
        sruUser.setEmail("s154263@student.pg.edu.pl");
        sruUser.setStudentId(154263);
        entityManager.persist(sruUser);

        VLAN vlanUser = new VLAN();
        vlanUser.setNumber(42);
        vlanUser.setName("SKOS użytkownicy");
        entityManager.persist(vlanUser);

        VLAN vlanAdministration = new VLAN();
        vlanAdministration.setNumber(53);
        vlanAdministration.setName("SKOS administracja");
        entityManager.persist(vlanAdministration);

        Subnet userPublic = new Subnet();
        userPublic.setDomain("ds.pg.gda.pl");
        userPublic.setName("Użytkownicy publiczne");
        userPublic.setVlan(vlanUser);
        entityManager.persist(userPublic);

        for (int i = 10; i < 15; i++) {
            for (int j = 1; j < 5; j++) {
                IPAddress ipAddress = new IPAddress();
                ipAddress.setIpType(IPAddress.IPType.IPv4);
                ipAddress.setAddress("153.19." + i + "." + j);
                ipAddress.setSubnet(userPublic);
                entityManager.persist(ipAddress);
            }
            IPAddress ipAddress = new IPAddress();
            ipAddress.setIpType(IPAddress.IPType.IPv6);
            ipAddress.setAddress("2001:4070:13::1:0:208:" + i);
            ipAddress.setSubnet(userPublic);
            entityManager.persist(ipAddress);
        }

        Subnet userPrivate = new Subnet();
        userPrivate.setName("Użytkownicy prywatne");
        userPrivate.setVlan(vlanUser);
        entityManager.persist(userPrivate);

        for (int i = 10; i < 15; i++) {
            for (int j = 1; j < 5; j++) {
                IPAddress ipAddress = new IPAddress();
                ipAddress.setIpType(IPAddress.IPType.IPv4);
                ipAddress.setAddress("10.42." + i + "." + j);
                ipAddress.setSubnet(userPrivate);
                entityManager.persist(ipAddress);
            }
            IPAddress ipAddress = new IPAddress();
            ipAddress.setIpType(IPAddress.IPType.IPv6);
            ipAddress.setAddress("fe80::8ae0:f300:3a60:" + i);
            ipAddress.setSubnet(userPrivate);
            entityManager.persist(ipAddress);
        }

        Subnet administration = new Subnet();
        administration.setDomain("pg.gda.pl");
        administration.setName("Administracja publiczne");
        administration.setVlan(vlanAdministration);
        entityManager.persist(administration);

        for (int i = 10; i < 15; i++) {
            for (int j = 1; j < 5; j++) {
                IPAddress ipAddress = new IPAddress();
                ipAddress.setIpType(IPAddress.IPType.IPv4);
                ipAddress.setAddress("153.20." + i + "." + j);
                ipAddress.setSubnet(administration);
                entityManager.persist(ipAddress);
            }
        }

        DeviceType computer = new DeviceType();
        computer.setCode("COMPUTER");
        computer.setSubnet(userPublic);
        entityManager.persist(computer);

        DeviceType mobile = new DeviceType();
        mobile.setCode("MOBILE_DEVICE");
        mobile.setSubnet(userPrivate);
        entityManager.persist(mobile);

        DeviceType router = new DeviceType();
        router.setCode("ROUTER");
        router.setSubnet(userPublic);
        entityManager.persist(router);

        DeviceType administrationDevice = new DeviceType();
        administrationDevice.setCode("SERVER");
        administrationDevice.setSubnet(administration);
        entityManager.persist(administrationDevice);

        for (int i = 1; i <= 12; i++) {
            Dormitory dormitory = new Dormitory();
            dormitory.setName("DS" + i);
            entityManager.persist(dormitory);

            for (int j = 100; j <= 100 + i; j++) {
                Room room = new Room();
                room.setPlaces(i % 3 + 1);
                room.setDormitory(dormitory);
                room.setName(String.valueOf(j));
                entityManager.persist(room);
            }
            Room room = new Room();
            room.setDormitory(dormitory);
            room.setName("kuchnia");
            room.setPlaces(0);
            entityManager.persist(room);
        }

        return new Object() {
            public final String uuid = sruUser.getUuid();
        };
    }
}
