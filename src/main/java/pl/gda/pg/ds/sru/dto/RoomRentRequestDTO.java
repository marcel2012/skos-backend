package pl.gda.pg.ds.sru.dto;

public class RoomRentRequestDTO {
    private String sruUserUuid;
    private String roomUuid;
    private String dormitoryUuid;

    public String getSruUserUuid() {
        return sruUserUuid;
    }

    public void setSruUserUuid(String sruUserUuid) {
        this.sruUserUuid = sruUserUuid;
    }

    public String getRoomUuid() {
        return roomUuid;
    }

    public void setRoomUuid(String roomUuid) {
        this.roomUuid = roomUuid;
    }

    public String getDormitoryUuid() {
        return dormitoryUuid;
    }

    public void setDormitoryUuid(String dormitoryUuid) {
        this.dormitoryUuid = dormitoryUuid;
    }
}
