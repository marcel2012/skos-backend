package pl.gda.pg.ds.sru.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class DeviceDHCPLease extends DHCPLease {
    @ManyToOne
    private Device device;

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}
