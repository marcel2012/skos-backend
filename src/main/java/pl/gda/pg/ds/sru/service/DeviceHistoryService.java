package pl.gda.pg.ds.sru.service;

import pl.gda.pg.ds.sru.dto.DeviceHistoryResponseDTO;
import pl.gda.pg.ds.sru.entity.DeviceHistory;
import pl.gda.pg.ds.sru.security.Secured;
import pl.gda.pg.ds.sru.security.SruRole;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class DeviceHistoryService {

    @Inject
    private UserService userService;
    @Inject
    private DeviceService deviceService;
    @PersistenceContext
    private EntityManager entityManager;

    private DeviceHistoryResponseDTO convertDeviceHistoryToDTO(DeviceHistory deviceHistory) {
        DeviceHistoryResponseDTO response = new DeviceHistoryResponseDTO();
        deviceService.fillDeviceResponseDTO(response, deviceHistory.getDevice());
        response.setModifiedBy(userService.convertUserToDTO(deviceHistory.getModifiedBy()));
        response.setValidFrom(deviceHistory.getValidFrom());
        return response;
    }

    @GET
    @Path("/device/{uuid}/history")
    @Secured({SruRole.ADMIN})
    public List<DeviceHistoryResponseDTO> getMyDevices(@PathParam("uuid") String uuid) {
        String jpql = "SELECT d " +
                "FROM DeviceHistory d " +
                "WHERE d.targetUuid = :uuid " +
                "ORDER BY d.validFrom DESC";
        TypedQuery<DeviceHistory> q = entityManager.createQuery(jpql, DeviceHistory.class);
        q.setParameter("uuid", uuid);
        List<DeviceHistory> devices = q.getResultList();
        return devices.stream().map(this::convertDeviceHistoryToDTO).collect(Collectors.toList());
    }
}
