package pl.gda.pg.ds.sru.exception;

public class ServiceException extends RuntimeException {
    private ExceptionType exceptionType;

    public ServiceException(ExceptionType exceptionType) {
        super(exceptionType.toString());
        this.exceptionType = exceptionType;
    }

    public ExceptionType getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(ExceptionType exceptionType) {
        this.exceptionType = exceptionType;
    }

    public enum ExceptionType {
        LOGIN_LENGTH, PASSWORD_LENGTH, EMAIL_FORMAT, LOGIN_UNIQUE, NAME_LENGTH, SURNAME_LENGTH, BAD_PASSWORD,
        BAD_MAC_ADDRESS, HOST_NAME_LENGTH, BAD_HOST_NAME, HOST_NAME_UNIQUE, BAD_DEVICE_TYPE, MAC_ADDRESS_UNIQUE,
        BAD_UUID, BAD_USER_TYPE, DORMITORY_NAME_LENGTH, BAD_DORMITORY_NAME, DORMITORY_NAME_UNIQUE, ROOM_NAME_LENGTH,
        BAD_ROOM_NAME, ROOM_NAME_UNIQUE, NO_FREE_IP_ADDRESS, BAD_ROOM_PLACES,
    }
}
