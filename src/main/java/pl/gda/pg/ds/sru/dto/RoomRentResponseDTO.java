package pl.gda.pg.ds.sru.dto;

import java.util.Date;

public class RoomRentResponseDTO {
    private String uuid;
    private Date validFrom;
    private Date validTo;
    private ProfileResponseDTO user;
    private RoomResponseDTO room;
    private DormitoryResponseDTO dormitory;

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ProfileResponseDTO getUser() {
        return user;
    }

    public void setUser(ProfileResponseDTO user) {
        this.user = user;
    }

    public RoomResponseDTO getRoom() {
        return room;
    }

    public void setRoom(RoomResponseDTO room) {
        this.room = room;
    }

    public DormitoryResponseDTO getDormitory() {
        return dormitory;
    }

    public void setDormitory(DormitoryResponseDTO dormitory) {
        this.dormitory = dormitory;
    }
}
