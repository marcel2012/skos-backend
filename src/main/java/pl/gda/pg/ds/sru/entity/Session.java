package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.BasicEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Session extends BasicEntity {

    @Column(unique = true)
    private String token;
    @ManyToOne
    private SruUser sruUser;
    private Date recentlyActive;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public SruUser getSruUser() {
        return sruUser;
    }

    public void setSruUser(SruUser sruUser) {
        this.sruUser = sruUser;
    }

    public Date getRecentlyActive() {
        return recentlyActive;
    }

    public void setRecentlyActive(Date recentlyActive) {
        this.recentlyActive = recentlyActive;
    }
}
