package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.BasicEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class DeviceType extends BasicEntity {
    @Column(nullable = false, unique = true)
    private String code;
    @ManyToOne
    @Column(nullable = false)
    private Subnet subnet;
    @OneToMany(mappedBy = "deviceType")
    private Set<Device> devices;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Subnet getSubnet() {
        return subnet;
    }

    public void setSubnet(Subnet subnet) {
        this.subnet = subnet;
    }
}
