package pl.gda.pg.ds.sru.service;

import pl.gda.pg.ds.sru.dto.RoomRentRequestDTO;
import pl.gda.pg.ds.sru.dto.RoomRentResponseDTO;
import pl.gda.pg.ds.sru.entity.Room;
import pl.gda.pg.ds.sru.entity.RoomRent;
import pl.gda.pg.ds.sru.entity.SruUser;
import pl.gda.pg.ds.sru.exception.ServiceException;
import pl.gda.pg.ds.sru.security.Secured;
import pl.gda.pg.ds.sru.security.SruRole;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.stream.Collectors;

import static pl.gda.pg.ds.sru.exception.ServiceException.ExceptionType.BAD_UUID;

@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class RoomRentService {

    @PersistenceContext
    private EntityManager entityManager;
    @Inject
    private UserService userService;
    @Inject
    private DormitoryService dormitoryService;

    @POST
    @Path("/roomRent")
    //Todo: usunąć admina
    @Secured({SruRole.ADMIN, SruRole.OFFICE})
    public Object createLease(@Context SecurityContext securityContext, RoomRentRequestDTO req) {
        SruUser modifiedBy = userService.getSruUser(securityContext);
        SruUser sruUser = userService.findUserByUuid(req.getSruUserUuid());
        if (sruUser == null) {
            throw new ServiceException(BAD_UUID);
        }
        Room room = dormitoryService.findRoomByUuidAndDormitory(req.getRoomUuid(), req.getDormitoryUuid());
        if (room == null) {
            throw new ServiceException(BAD_UUID);
        }
        RoomRent roomRent = new RoomRent();
        roomRent.setRoom(room);
        roomRent.setSruUser(sruUser);
        roomRent.setCreatedBy(modifiedBy);
        entityManager.persist(roomRent);
        return new Object() {
            public final String uuid = roomRent.getUuid();
        };
    }

    @DELETE
    @Path("/roomRent/{uuid}")
    //Todo: usunąć admina
    @Secured({SruRole.ADMIN, SruRole.OFFICE})
    public Object deleteLease(@Context SecurityContext securityContext, @PathParam("uuid") String uuid) {
        SruUser modifiedBy = userService.getSruUser(securityContext);
        cancelRent(uuid, modifiedBy);
        return new Object();
    }

    public void cancelRent(String uuid, SruUser modifiedBy) {
        String jpql = "UPDATE RoomRent r " +
                "SET r.validTo = CURRENT_TIMESTAMP, " +
                "r.canceledBy = :modifiedBy " +
                "WHERE r.uuid = :uuid " +
                "  AND (r.validTo IS NULL OR r.validTo > CURRENT_TIMESTAMP)";
        Query q = entityManager.createQuery(jpql);
        q.setParameter("uuid", uuid);
        q.setParameter("modifiedBy", modifiedBy);
        int editedRows = q.executeUpdate();
        if (editedRows == 0) {
            throw new ServiceException(BAD_UUID);
        }
    }

    @GET
    @Path("/user/me/roomRent")
    @Secured({SruRole.USER})
    public List<RoomRentResponseDTO> getActiveLeases(@Context SecurityContext securityContext) {
        SruUser sruUser = userService.getSruUser(securityContext);
        return getActiveLeasesForUser(sruUser);
    }

    @GET
    @Path("/user/{userUuid}/roomRent")
    @Secured({SruRole.ADMIN, SruRole.OFFICE})
    public List<RoomRentResponseDTO> getUserActiveLeases(@PathParam("userUuid") String userUuid) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        if (sruUser == null) {
            throw new ServiceException(BAD_UUID);
        }
        return getActiveLeasesForUser(sruUser);
    }

    @GET
    @Path("/user/{userUuid}/roomRent/inactive")
    @Secured({SruRole.ADMIN, SruRole.OFFICE})
    public List<RoomRentResponseDTO> getUserInactiveLeases(@PathParam("userUuid") String userUuid) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        if (sruUser == null) {
            throw new ServiceException(BAD_UUID);
        }
        return getInactiveLeasesForUser(sruUser);
    }

    private List<RoomRentResponseDTO> getActiveLeasesForUser(SruUser sruUser) {
        return getActiveRentsForSruUser(sruUser).stream().map(this::convertRootRentToDTO).collect(Collectors.toList());
    }

    private List<RoomRentResponseDTO> getInactiveLeasesForUser(SruUser sruUser) {
        return getInactiveRentsForSruUser(sruUser).stream().map(this::convertRootRentToDTO).collect(Collectors.toList());
    }

    private RoomRentResponseDTO convertRootRentToDTO(RoomRent roomRent) {
        RoomRentResponseDTO response = new RoomRentResponseDTO();
        response.setUuid(roomRent.getUuid());
        response.setValidFrom(roomRent.getValidFrom());
        response.setValidTo(roomRent.getValidTo());
        response.setDormitory(dormitoryService.convertDormitoryToDTO(roomRent.getRoom().getDormitory()));
        response.setRoom(dormitoryService.convertRoomToDTO(roomRent.getRoom()));
        response.setUser(userService.convertUserToDTO(roomRent.getSruUser()));
        return response;
    }

    public List<RoomRent> getActiveRentsForRoom(Room room) {
        String jpql = "SELECT r " +
                "FROM RoomRent r " +
                "WHERE r.validFrom <= CURRENT_TIMESTAMP " +
                "  AND (r.validTo IS NULL OR r.validTo >= CURRENT_TIMESTAMP) " +
                "  AND r.room = :room " +
                "ORDER BY r.validFrom, r.uuid";
        TypedQuery<RoomRent> q = entityManager.createQuery(jpql, RoomRent.class);
        q.setParameter("room", room);
        return q.getResultList();
    }

    public List<RoomRent> getActiveRentsForSruUser(SruUser sruUser) {
        String jpql = "SELECT r " +
                "FROM RoomRent r " +
                "WHERE r.validFrom <= CURRENT_TIMESTAMP " +
                "  AND (r.validTo IS NULL OR r.validTo >= CURRENT_TIMESTAMP) " +
                "  AND r.sruUser = :sruUser " +
                "ORDER BY r.validFrom, r.uuid";
        TypedQuery<RoomRent> q = entityManager.createQuery(jpql, RoomRent.class);
        q.setParameter("sruUser", sruUser);
        return q.getResultList();
    }

    public List<RoomRent> getInactiveRentsForSruUser(SruUser sruUser) {
        String jpql = "SELECT r " +
                "FROM RoomRent r " +
                "WHERE (r.validFrom > CURRENT_TIMESTAMP OR r.validTo < CURRENT_TIMESTAMP) " +
                "  AND r.sruUser = :sruUser " +
                "ORDER BY r.validFrom, r.uuid";
        TypedQuery<RoomRent> q = entityManager.createQuery(jpql, RoomRent.class);
        q.setParameter("sruUser", sruUser);
        return q.getResultList();
    }
}
