package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.BasicEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Subnet extends BasicEntity {
    @Column(nullable = false, unique = true)
    private String name;
    private String domain;
    @ManyToOne
    @Column(nullable = false)
    private VLAN vlan;
    @OneToMany(mappedBy = "subnet")
    private Set<IPAddress> ipAddresses;
    @OneToMany(mappedBy = "subnet")
    private Set<DeviceType> deviceTypes;

    public VLAN getVlan() {
        return vlan;
    }

    public void setVlan(VLAN vlan) {
        this.vlan = vlan;
    }

    public Set<IPAddress> getIpAddresses() {
        return ipAddresses;
    }

    public void setIpAddresses(Set<IPAddress> ipAddresses) {
        this.ipAddresses = ipAddresses;
    }

    public Set<DeviceType> getDeviceTypes() {
        return deviceTypes;
    }

    public void setDeviceTypes(Set<DeviceType> deviceTypes) {
        this.deviceTypes = deviceTypes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
}
