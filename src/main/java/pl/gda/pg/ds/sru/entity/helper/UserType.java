package pl.gda.pg.ds.sru.entity.helper;

public enum UserType {
    DEFAULT, STUDENT, ADMINISTRATOR, DORMITORY_OFFICE, RECEPTION
}
