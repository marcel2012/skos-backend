package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.BasicEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Room extends BasicEntity implements Comparable<Room> {

    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private Integer places;
    @ManyToOne
    @Column(nullable = false)
    private Dormitory dormitory;
    @OneToMany(mappedBy = "room")
    private Set<RoomRent> roomRents;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dormitory getDormitory() {
        return dormitory;
    }

    public void setDormitory(Dormitory dormitory) {
        this.dormitory = dormitory;
    }

    public Integer getPlaces() {
        return places;
    }

    public void setPlaces(Integer places) {
        this.places = places;
    }

    public Set<RoomRent> getRoomRents() {
        return roomRents;
    }

    public void setRoomRents(Set<RoomRent> roomRents) {
        this.roomRents = roomRents;
    }

    @Override
    public int compareTo(Room room) {
        return name.compareTo(room.name);
    }
}
