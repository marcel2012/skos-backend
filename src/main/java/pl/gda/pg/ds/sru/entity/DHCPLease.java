package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.LeaseDataEntity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class DHCPLease extends LeaseDataEntity {
    @ManyToOne
    @Column(nullable = false)
    private IPAddress ipAddress;

    public IPAddress getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(IPAddress ipAddress) {
        this.ipAddress = ipAddress;
    }
}
