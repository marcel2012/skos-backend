package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.BasicEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class IPAddress extends BasicEntity {
    @Column(nullable = false, unique = true)
    private String address;
    @ManyToOne
    @Column(nullable = false)
    private Subnet subnet;
    @Column(nullable = false)
    private IPType ipType;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public IPType getIpType() {
        return ipType;
    }

    public void setIpType(IPType ipType) {
        this.ipType = ipType;
    }

    public Subnet getSubnet() {
        return subnet;
    }

    public void setSubnet(Subnet subnet) {
        this.subnet = subnet;
    }

    public enum IPType {IPv4, IPv6}

}
