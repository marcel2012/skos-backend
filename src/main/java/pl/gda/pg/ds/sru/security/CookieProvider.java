package pl.gda.pg.ds.sru.security;

import pl.gda.pg.ds.sru.entity.Session;
import pl.gda.pg.ds.sru.entity.SruUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

import static pl.gda.pg.ds.sru.security.Authentication.sessionExpirationMinutes;

@Stateless
public class CookieProvider {
    private static final String COOKIE_NAME = "SESSION_TOKEN";
    private static final String API_PATH = "/api/";
    private static final String API_DOMAIN = System.getenv("DOMAIN");
    private static final int tokenBits = 512;

    @PersistenceContext
    private EntityManager entityManager;

    public Cookie getCookie(ContainerRequestContext requestContext) {
        return requestContext.getCookies().get(COOKIE_NAME);
    }

    private String generateToken() {
        Random random = new SecureRandom();
        return new BigInteger(tokenBits, random).toString(Character.MAX_RADIX);
    }

    public NewCookie createSessionCookie(SruUser sruUser) {
        String sessionToken = generateToken();
        Session session = new Session();
        session.setToken(sessionToken);
        session.setSruUser(sruUser);
        session.setRecentlyActive(new Date());
        entityManager.persist(session);
        return new NewCookie(COOKIE_NAME, sessionToken, API_PATH, API_DOMAIN, 1, "", sessionExpirationMinutes * 60, null, false, true);
    }

    public NewCookie createLogoutCookie(SruUser sruUser) {
        return new NewCookie(COOKIE_NAME, "", API_PATH, API_DOMAIN, 1, "", -1, null, false, true);
    }
}
