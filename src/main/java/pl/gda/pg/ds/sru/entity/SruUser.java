package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.BasicEntity;
import pl.gda.pg.ds.sru.entity.helper.BPassword;
import pl.gda.pg.ds.sru.entity.helper.UserType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Embeddable
public class SruUser extends BasicEntity implements Serializable, Comparable<SruUser> {

    @Column(nullable = false)
    private UserType userType = UserType.DEFAULT;
    private String login;
    @Embedded
    private BPassword bPassword;
    @OneToMany(mappedBy = "sruUser")
    private Set<Device> devices = new HashSet<>();
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    private Integer studentId;
    @OneToMany(mappedBy = "sruUser")
    private Set<RoomRent> roomRents = new HashSet<>();
    private String email;
    @OneToMany(mappedBy = "sruUser")
    private Set<SruUserDHCPLease> dhcpLeases;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public BPassword getbPassword() {
        return bPassword;
    }

    public void setbPassword(BPassword bPassword) {
        this.bPassword = bPassword;
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public void setDevices(Set<Device> devices) {
        this.devices = devices;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Set<SruUserDHCPLease> getDhcpLeases() {
        return dhcpLeases;
    }

    public void setDhcpLeases(Set<SruUserDHCPLease> dhcpLeases) {
        this.dhcpLeases = dhcpLeases;
    }

    @Override
    public int compareTo(SruUser sruUser) {
        return uuid.compareTo(sruUser.getUuid());
    }

    public Set<RoomRent> getRoomRents() {
        return roomRents;
    }

    public void setRoomRents(Set<RoomRent> roomRents) {
        this.roomRents = roomRents;
    }
}
