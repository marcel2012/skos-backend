package pl.gda.pg.ds.sru.entity.helper;

import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.Embeddable;

@Embeddable
public class BPassword {

    private String bPassword;

    public BPassword() {
    }

    public BPassword(String password) {
        setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
    }

    public String getPassword() {
        return bPassword;
    }

    public void setPassword(String bPassword) {
        this.bPassword = bPassword;
    }

    public boolean equalsHashes(String password) {
        return BCrypt.checkpw(password, bPassword);
    }
}
