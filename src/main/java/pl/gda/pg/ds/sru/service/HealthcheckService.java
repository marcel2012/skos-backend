package pl.gda.pg.ds.sru.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import static pl.gda.pg.ds.sru.service.helper.QueryResult.getElementFromQuery;

@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Path("/health")
public class HealthcheckService {
    @PersistenceContext
    private EntityManager entityManager;

    @Path("/db")
    @GET
    public Object databaseHealthCheck() {
        String jpql = "SELECT COUNT(u) " +
                "FROM SruUser u";
        TypedQuery<Long> q = entityManager.createQuery(jpql, Long.class);
        return new Object() {
            public final String response = "ok";
            public final Long count = getElementFromQuery(q);
        };
    }

    @Path("/")
    @GET
    public Object healthCheck() {
        return new Object() {
            public final String response = "ok";
        };
    }
}
