package pl.gda.pg.ds.sru.security;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Provider
@Secured
@Priority(Priorities.AUTHORIZATION)
public class Authorization implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        Class<?> resourceClass = resourceInfo.getResourceClass();
        List<SruRole> classRoles = extractRoles(resourceClass);

        Method resourceMethod = resourceInfo.getResourceMethod();
        List<SruRole> methodRoles = extractRoles(resourceMethod);

        try {
            if (methodRoles.isEmpty()) {
                checkPermissions(classRoles, requestContext);
            } else {
                checkPermissions(methodRoles, requestContext);
            }
        } catch (SecurityException e) {
            requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
        } catch (Exception e) {
            e.printStackTrace();
            requestContext.abortWith(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
        }
    }

    private List<SruRole> extractRoles(AnnotatedElement annotatedElement) {
        if (annotatedElement != null) {
            Secured secured = annotatedElement.getAnnotation(Secured.class);
            if (secured != null) {
                SruRole[] allowedRoles = secured.value();
                return Arrays.asList(allowedRoles);
            }
        }
        return new ArrayList<>();
    }

    private void checkPermissions(List<SruRole> allowedRoles, ContainerRequestContext requestContext) {
        SecurityContext securityContext = requestContext.getSecurityContext();
        if (allowedRoles.size() > 0) {
            if (allowedRoles.stream().map(SruRole::toString).noneMatch(securityContext::isUserInRole)) {
                throw new SecurityException();
            }
        }
    }
}
