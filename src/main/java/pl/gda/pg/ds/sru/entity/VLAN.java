package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.BasicEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class VLAN extends BasicEntity {
    @Column(nullable = false, unique = true)
    private int number;
    @Column(nullable = false, unique = true)
    private String name;
    @OneToMany(mappedBy = "vlan")
    private Set<Subnet> subnets;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Subnet> getSubnets() {
        return subnets;
    }

    public void setSubnets(Set<Subnet> subnets) {
        this.subnets = subnets;
    }
}
