package pl.gda.pg.ds.sru.entity.base;

import pl.gda.pg.ds.sru.entity.SruUser;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import java.util.Date;

@MappedSuperclass
public class LeaseDataEntity extends BasicEntity {
    @Column(nullable = false)
    private Date validFrom = new Date();
    private Date validTo;
    @OneToOne
    @Column(nullable = false)
    private SruUser createdBy;
    @OneToOne
    private SruUser canceledBy;

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public SruUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(SruUser createdBy) {
        this.createdBy = createdBy;
    }

    public SruUser getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(SruUser canceledBy) {
        this.canceledBy = canceledBy;
    }
}
