package pl.gda.pg.ds.sru.service;

import pl.gda.pg.ds.sru.dto.SruUserHistoryResponseDTO;
import pl.gda.pg.ds.sru.entity.SruUserHistory;
import pl.gda.pg.ds.sru.security.Secured;
import pl.gda.pg.ds.sru.security.SruRole;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
public class UserHistoryService {

    @Inject
    private UserService userService;
    @PersistenceContext
    private EntityManager entityManager;

    private SruUserHistoryResponseDTO convertSruUserHistoryToDTO(SruUserHistory sruUserHistory) {
        SruUserHistoryResponseDTO response = new SruUserHistoryResponseDTO();
        userService.fillSruUserResponseDTO(response, sruUserHistory.getSruUser());
        response.setModifiedBy(userService.convertUserToDTO(sruUserHistory.getModifiedBy()));
        response.setValidFrom(sruUserHistory.getValidFrom());
        return response;
    }

    @GET
    @Path("/user/{uuid}/history")
    @Secured({SruRole.ADMIN, SruRole.OFFICE})
    public List<SruUserHistoryResponseDTO> getMyDevices(@PathParam("uuid") String uuid) {
        String jpql = "SELECT u " +
                "FROM SruUserHistory u " +
                "WHERE u.targetUuid = :uuid " +
                "ORDER BY u.validFrom DESC";
        TypedQuery<SruUserHistory> q = entityManager.createQuery(jpql, SruUserHistory.class);
        q.setParameter("uuid", uuid);
        List<SruUserHistory> devices = q.getResultList();
        return devices.stream().map(this::convertSruUserHistoryToDTO).collect(Collectors.toList());
    }
}
