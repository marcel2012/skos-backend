package pl.gda.pg.ds.sru.service;

import pl.gda.pg.ds.sru.dto.DHCPLeaseRequestDTO;
import pl.gda.pg.ds.sru.dto.DHCPLeaseResponseDTO;
import pl.gda.pg.ds.sru.dto.IPAddressResponseDTO;
import pl.gda.pg.ds.sru.entity.*;
import pl.gda.pg.ds.sru.exception.ServiceException;
import pl.gda.pg.ds.sru.security.Secured;
import pl.gda.pg.ds.sru.security.SruRole;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.stream.Collectors;

import static pl.gda.pg.ds.sru.exception.ServiceException.ExceptionType.BAD_UUID;
import static pl.gda.pg.ds.sru.exception.ServiceException.ExceptionType.NO_FREE_IP_ADDRESS;
import static pl.gda.pg.ds.sru.service.helper.QueryResult.getElementFromQuery;

@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class DHCPService {
    private static final int itemsPerPage = 2;
    @PersistenceContext
    private EntityManager entityManager;
    @Inject
    private UserService userService;
    @Inject
    private DeviceService deviceService;

    @GET
    @Path("/dhcp")
    @Secured({SruRole.ADMIN})
    @Produces("text/plain")
    public String getActiveDeviceDHCPLeases() {
        String jpql = "SELECT d " +
                "FROM DeviceDHCPLease d " +
                "WHERE d.validFrom <= CURRENT_TIMESTAMP " +
                "  AND (d.validTo IS NULL OR d.validTo >= CURRENT_TIMESTAMP) " +
                "ORDER BY d.uuid";
        TypedQuery<DeviceDHCPLease> deviceQ = entityManager.createQuery(jpql, DeviceDHCPLease.class);
        List<DeviceDHCPLease> deviceDhcpLeases = deviceQ.getResultList();
        return deviceDhcpLeases.stream().map(this::convertDeviceDHCPLeaseToDHCPConfig).collect(Collectors.joining());
    }

    public List<IPAddress> getFreeIPAddressesBySubnet(Subnet subnet) {
        String jpql = "SELECT ip " +
                "FROM IPAddress ip " +
                "WHERE ip.subnet = :subnet " +
                "  AND NOT EXISTS(SELECT dhcp " +
                "                 FROM DHCPLease dhcp " +
                "                 WHERE dhcp.ipAddress = ip " +
                "                   AND dhcp.validFrom <= CURRENT_TIMESTAMP " +
                "                   AND (dhcp.validTo IS NULL OR dhcp.validTo >= CURRENT_TIMESTAMP)) " +
                "ORDER BY ip.address";
        TypedQuery<IPAddress> q = entityManager.createQuery(jpql, IPAddress.class);
        q.setParameter("subnet", subnet);
        return q.getResultList();
    }

    public VLAN getVLANByNumber(int number) {
        String jpql = "SELECT v " +
                "FROM VLAN v " +
                "WHERE v.number = :number";
        TypedQuery<VLAN> q = entityManager.createQuery(jpql, VLAN.class);
        q.setParameter("number", number);
        return getElementFromQuery(q);
    }

    @DELETE
    @Path("/user/me/device/{uuid}/dhcp")
    @Secured({SruRole.USER})
    public Object deactivateDHCPLeasesForMyDevice(@Context SecurityContext securityContext, @PathParam("uuid") String uuid) {
        SruUser sruUser = userService.getSruUser(securityContext);
        deactivateDeviceDhcpLeases(sruUser, sruUser, uuid);
        return new Object();
    }

    @DELETE
    @Path("/user/{userUuid}/device/{uuid}/dhcp")
    @Secured({SruRole.ADMIN})
    public Object deactivateDHCPLeasesForDevice(@Context SecurityContext securityContext, @PathParam("userUuid") String userUuid, @PathParam("uuid") String uuid) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        SruUser modifiedBy = userService.getSruUser(securityContext);
        deactivateDeviceDhcpLeases(sruUser, modifiedBy, uuid);
        return new Object();
    }

    private void deactivateDeviceDhcpLeases(SruUser sruUser, SruUser modifiedBy, String uuid) {
        Device device = deviceService.findDeviceByUuidAndUser(uuid, sruUser);
        if (device == null) {
            throw new ServiceException(BAD_UUID);
        }
        cancelDHCPLeasesForDevice(device, modifiedBy);
    }

    public void cancelDHCPLeasesForDevice(Device device, SruUser modifiedBy) {
        String jpql = "UPDATE DHCPLease d " +
                "SET d.validTo = CURRENT_TIMESTAMP, " +
                "d.canceledBy = :modifiedBy " +
                "WHERE (SELECT dev.device FROM DeviceDHCPLease dev WHERE dev.uuid = d.uuid) = :device " +
                "  AND (d.validTo IS NULL OR d.validTo > CURRENT_TIMESTAMP)";
        Query q = entityManager.createQuery(jpql);
        q.setParameter("device", device);
        q.setParameter("modifiedBy", modifiedBy);
        int editedRows = q.executeUpdate();
        if (editedRows == 0) {
            throw new ServiceException(BAD_UUID);
        }
    }

    public void cancelDHCPLeases(String uuid, SruUser modifiedBy) {
        String jpql = "UPDATE DHCPLease d " +
                "SET d.validTo = CURRENT_TIMESTAMP, " +
                "d.canceledBy = :modifiedBy " +
                "WHERE d.uuid = :uuid " +
                "  AND (d.validTo IS NULL OR d.validTo > CURRENT_TIMESTAMP)";
        Query q = entityManager.createQuery(jpql);
        q.setParameter("uuid", uuid);
        q.setParameter("modifiedBy", modifiedBy);
        int editedRows = q.executeUpdate();
        if (editedRows == 0) {
            throw new ServiceException(BAD_UUID);
        }
    }

    @POST
    @Path("/user/me/device/{uuid}/dhcp")
    @Secured({SruRole.USER})
    public Object createLease(@Context SecurityContext securityContext, @PathParam("uuid") String uuid) {
        SruUser sruUser = userService.getSruUser(securityContext);
        return createLease(sruUser, sruUser, uuid, null);
    }

    @POST
    @Path("/user/{userUuid}/device/{uuid}/dhcp")
    @Secured({SruRole.ADMIN})
    public Object createLease(@Context SecurityContext securityContext, @PathParam("userUuid") String userUuid, @PathParam("uuid") String uuid, DHCPLeaseRequestDTO req) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        SruUser modifiedBy = userService.getSruUser(securityContext);
        return createLease(sruUser, modifiedBy, uuid, req.getIpAddressUuid());
    }

    private Object createLease(SruUser sruUser, SruUser modifiedBy, String uuid, String ipAddressUuid) {
        Device device = deviceService.findDeviceByUuidAndUser(uuid, sruUser);
        if (device == null) {
            throw new ServiceException(BAD_UUID);
        }
        Subnet subnet = device.getDeviceType().getSubnet();
        IPAddress ipAddress;
        List<IPAddress> ipAddresses = getFreeIPAddressesBySubnet(subnet);
        if (ipAddresses == null || ipAddresses.size() == 0) {
            throw new ServiceException(NO_FREE_IP_ADDRESS);
        }
        if (ipAddressUuid != null) {
            List<IPAddress> filteredIPAddresses = ipAddresses.stream().filter(ip -> ip.getUuid().equals(ipAddressUuid)).collect(Collectors.toList());
            if (filteredIPAddresses.size() != 1) {
                throw new ServiceException(NO_FREE_IP_ADDRESS);
            }
            ipAddress = filteredIPAddresses.get(0);
        } else {
            ipAddress = ipAddresses.get(0);
        }
        DeviceDHCPLease dhcpLease = new DeviceDHCPLease();
        dhcpLease.setIpAddress(ipAddress);
        dhcpLease.setDevice(device);
        dhcpLease.setCreatedBy(modifiedBy);
        entityManager.persist(dhcpLease);
        return new Object() {
            public final String uuid = dhcpLease.getUuid();
        };
    }

    @GET
    @Path("/user/{userUuid}/device/{uuid}/ip/search")
    @Secured({SruRole.ADMIN})
    public Object getFreeIPAddresses(@PathParam("userUuid") String userUuid,
                                     @PathParam("uuid") String uuid, @QueryParam("ipAddress") String ipAddress,
                                     @QueryParam("page") Integer page) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        Device device = deviceService.findDeviceByUuidAndUser(uuid, sruUser);
        if (device == null) {
            throw new ServiceException(BAD_UUID);
        }
        Subnet subnet = device.getDeviceType().getSubnet();
        String jpql = "FROM IPAddress ip " +
                "WHERE ip.subnet = :subnet " +
                "  AND NOT EXISTS(SELECT dhcp " +
                "                 FROM DHCPLease dhcp " +
                "                 WHERE dhcp.ipAddress = ip " +
                "                   AND dhcp.validFrom <= CURRENT_TIMESTAMP " +
                "                   AND (dhcp.validTo IS NULL OR dhcp.validTo >= CURRENT_TIMESTAMP)) ";
        if (ipAddress != null) {
            jpql += "  AND ip.address LIKE :ipAddress ";
        }
        TypedQuery<IPAddress> query = entityManager.createQuery("SELECT ip " + jpql + "ORDER BY ip.address", IPAddress.class);
        TypedQuery<Long> queryCount = entityManager.createQuery("SELECT COUNT(ip) " + jpql, Long.class);
        Query[] queries = {query, queryCount};
        for (Query q : queries) {
            if (ipAddress != null) {
                q.setParameter("ipAddress", "%" + ipAddress + "%");
            }
            q.setParameter("subnet", subnet);
        }
        if (page != null) {
            query.setFirstResult(page * itemsPerPage);
            query.setMaxResults(itemsPerPage);
        }
        List<IPAddress> dhcpIPAddresses = query.getResultList();
        List<IPAddressResponseDTO> response = dhcpIPAddresses.stream().map(this::convertIPAddressToDTO).collect(Collectors.toList());
        Long count = queryCount.getSingleResult();
        return new Object() {
            public final List<IPAddressResponseDTO> ipAddresses = response;
            public final int pages = (int) Math.ceil((double) count / itemsPerPage);
        };
    }

    @DELETE
    @Path("/dhcp/{dhcpUuid}")
    @Secured({SruRole.ADMIN})
    public Object deleteLease(@Context SecurityContext securityContext, @PathParam("dhcpUuid") String dhcpUuid) {
        SruUser modifiedBy = userService.getSruUser(securityContext);
        cancelDHCPLeases(dhcpUuid, modifiedBy);
        return new Object();
    }

    @GET
    @Path("/user/me/device/{uuid}/dhcp")
    @Secured({SruRole.USER})
    public List<DHCPLeaseResponseDTO> getMyDeviceDhcp(@Context SecurityContext securityContext, @PathParam("uuid") String uuid) {
        SruUser sruUser = userService.getSruUser(securityContext);
        return getDeviceDhcpLease(sruUser, uuid);
    }

    @GET
    @Path("/user/{userUuid}/device/{uuid}/dhcp")
    @Secured({SruRole.ADMIN})
    public List<DHCPLeaseResponseDTO> getDeviceDhcp(@PathParam("userUuid") String userUuid, @PathParam("uuid") String uuid) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        return getDeviceDhcpLease(sruUser, uuid);
    }

    private List<DHCPLeaseResponseDTO> getDeviceDhcpLease(SruUser sruUser, String uuid) {
        Device device = deviceService.findDeviceByUuidAndUser(uuid, sruUser);
        if (device == null) {
            throw new ServiceException(BAD_UUID);
        }
        List<DeviceDHCPLease> dhcpLeases = getActiveDHCPLeasesForDevice(device);
        return dhcpLeases.stream().map(this::convertDHCPLeaseToDTO).collect(Collectors.toList());
    }

    @GET
    @Path("/user/{userUuid}/device/{uuid}/dhcp/inactive")
    @Secured({SruRole.ADMIN})
    public List<DHCPLeaseResponseDTO> getDeviceInactiveLeases(@PathParam("userUuid") String userUuid, @PathParam("uuid") String uuid) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        Device device = deviceService.findDeviceByUuidAndUser(uuid, sruUser);
        if (device == null) {
            throw new ServiceException(BAD_UUID);
        }
        List<DeviceDHCPLease> dhcpLeases = getInactiveDHCPLeasesForDevice(device);
        return dhcpLeases.stream().map(this::convertDHCPLeaseToDTO).collect(Collectors.toList());
    }

    private DHCPLeaseResponseDTO convertDHCPLeaseToDTO(DHCPLease dhcpLease) {
        DHCPLeaseResponseDTO response = new DHCPLeaseResponseDTO();
        response.setUuid(dhcpLease.getUuid());
        response.setIpAddress(dhcpLease.getIpAddress().getAddress());
        response.setValidFrom(dhcpLease.getValidFrom());
        response.setValidTo(dhcpLease.getValidTo());
        return response;
    }

    private IPAddressResponseDTO convertIPAddressToDTO(IPAddress ipAddress) {
        IPAddressResponseDTO response = new IPAddressResponseDTO();
        response.setUuid(ipAddress.getUuid());
        response.setAddress(ipAddress.getAddress());
        response.setIpType(ipAddress.getIpType().toString());
        return response;
    }

    public List<DeviceDHCPLease> getActiveDHCPLeasesForDevice(Device device) {
        String jpql = "SELECT d " +
                "FROM DeviceDHCPLease d " +
                "WHERE d.validFrom <= CURRENT_TIMESTAMP " +
                "  AND (d.validTo IS NULL OR d.validTo >= CURRENT_TIMESTAMP) " +
                "  AND d.device = :device " +
                "ORDER BY d.validFrom, d.uuid";
        TypedQuery<DeviceDHCPLease> q = entityManager.createQuery(jpql, DeviceDHCPLease.class);
        q.setParameter("device", device);
        return q.getResultList();
    }

    public List<DeviceDHCPLease> getInactiveDHCPLeasesForDevice(Device device) {
        String jpql = "SELECT d " +
                "FROM DeviceDHCPLease d " +
                "WHERE (d.validFrom > CURRENT_TIMESTAMP OR d.validTo < CURRENT_TIMESTAMP) " +
                "  AND d.device = :device " +
                "ORDER BY d.validFrom DESC, d.uuid";
        TypedQuery<DeviceDHCPLease> q = entityManager.createQuery(jpql, DeviceDHCPLease.class);
        q.setParameter("device", device);
        return q.getResultList();
    }

    public String convertDeviceDHCPLeaseToDHCPConfig(DeviceDHCPLease dhcpLease) {
        String output = dhcpLease.getDevice().getHostName() + " { ";
        output += dhcpLease.getDevice().getMacAddress() + " " + dhcpLease.getIpAddress().getAddress() + " ";
        output += "} ";
        return output;
    }
}
