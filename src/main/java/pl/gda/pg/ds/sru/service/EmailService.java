package pl.gda.pg.ds.sru.service;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Date;

@Path("/email")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class EmailService {
    @Resource
    private Session session;

    @GET
    @Path("/")
    public void sendEmail() {
        try {
            final MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(System.getProperty("mail.address")));
            final InternetAddress[] address = {new InternetAddress(System.getProperty("mail.debug"))};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("JavaMail API test");
            msg.setSentDate(new Date());
            msg.setText("test ąęś„źżłó", "UTF-8");
            Transport.send(msg);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
