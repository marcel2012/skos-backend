package pl.gda.pg.ds.sru.service;

import pl.gda.pg.ds.sru.dto.LoginRequestDTO;
import pl.gda.pg.ds.sru.dto.ProfileResponseDTO;
import pl.gda.pg.ds.sru.dto.RegistrationRequestDTO;
import pl.gda.pg.ds.sru.entity.SruUser;
import pl.gda.pg.ds.sru.entity.SruUserHistory;
import pl.gda.pg.ds.sru.entity.helper.UserType;
import pl.gda.pg.ds.sru.exception.ServiceException;
import pl.gda.pg.ds.sru.security.CookieProvider;
import pl.gda.pg.ds.sru.security.Secured;
import pl.gda.pg.ds.sru.security.SruRole;
import pl.gda.pg.ds.sru.service.helper.StringUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;
import java.util.stream.Collectors;

import static pl.gda.pg.ds.sru.exception.ServiceException.ExceptionType.BAD_UUID;
import static pl.gda.pg.ds.sru.service.helper.QueryResult.getElementFromQuery;

@Stateless
@Produces(MediaType.APPLICATION_JSON)
public class UserService {
    private static final int minPasswordLength = 6;
    private static final int itemsPerPage = 2;

    @Inject
    private CookieProvider cookieProvider;
    @PersistenceContext
    private EntityManager entityManager;
/*

    private boolean isValidEmailAddress(String email) {
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            return false;
        }
        return true;
    }*/

    public SruUser getSruUser(SecurityContext securityContext) {
        String uuid = securityContext.getUserPrincipal().getName();
        return findUserByUuid(uuid);
    }

    @PUT
    @Path("/user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Secured(SruRole.USER)
    public Object updateUser(@Context SecurityContext securityContext, RegistrationRequestDTO reg) {
        SruUser modifiedBy = getSruUser(securityContext);
        if (reg.getName() == null || reg.getName().length() < 1) {
            throw new ServiceException(ServiceException.ExceptionType.NAME_LENGTH);
        } else if (reg.getSurname() == null || reg.getSurname().length() < 1) {
            throw new ServiceException(ServiceException.ExceptionType.SURNAME_LENGTH);
        } /*else if (reg.getPassword() == null || reg.getPassword().length() < minPasswordLength) {
            throw new ServiceException(ServiceException.ExceptionType.PASSWORD_LENGTH);
        } else if (reg.getLogin() == null || reg.getLogin().length() < 1) {
            throw new ServiceException(ServiceException.ExceptionType.LOGIN_LENGTH);
        } *//*else if (reg.getEmail() == null || reg.getEmail().length() < 1 || !isValidEmailAddress(reg.getEmail())) {
                throw new ServiceException(ServiceException.ExceptionType.EMAIL_FORMAT);
            }*/ /*else if (findUserByLogin(reg.getLogin()) != null) {
            throw new ServiceException(ServiceException.ExceptionType.LOGIN_UNIQUE);
        }*/ else if (reg.getUserType() == null) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_USER_TYPE);
        }
        UserType userType;
        try {
            userType = UserType.valueOf(reg.getUserType());
        } catch (IllegalArgumentException exception) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_USER_TYPE);
        }
        SruUser sruUser;
        if (reg.getUuid() != null) {
            sruUser = findUserByUuid(reg.getUuid());
            if (sruUser == null) {
                throw new ServiceException(ServiceException.ExceptionType.BAD_UUID);
            }
        } else {
            sruUser = new SruUser();
        }
        sruUser.setFirstName(reg.getName());
        sruUser.setLastName(reg.getSurname());
        sruUser.setUserType(userType);
        //sruUser.setLogin(reg.getLogin());
        //sruUser.setbPassword(new BPassword(reg.getPassword()));
        //sruUser.setEmail(reg.getEmail());
        entityManager.persist(sruUser);
        saveUserToHistory(sruUser, modifiedBy);
        return new Object() {
            public final String uuid = sruUser.getUuid();
        };
    }

    @GET
    @Path("/user/me")
    @Secured({SruRole.USER})
    public ProfileResponseDTO getMyProfile(@Context SecurityContext securityContext) {
        SruUser sruUser = getSruUser(securityContext);
        return getUser(sruUser);
    }

    @GET
    @Path("/user/{uuid}")
    @Secured({SruRole.ADMIN, SruRole.OFFICE})
    public ProfileResponseDTO getUserProfile(@PathParam("uuid") String uuid) {
        SruUser sruUser = findUserByUuid(uuid);
        return getUser(sruUser);
    }

    private ProfileResponseDTO getUser(SruUser sruUser) {
        if (sruUser != null) {
            return convertUserToDTO(sruUser);
        }
        throw new ServiceException(BAD_UUID);
    }

    @GET
    @Path("/user/search")
    @Secured({SruRole.ADMIN, SruRole.OFFICE})
    public Object getProfile(@QueryParam("name") String name, @QueryParam("surname") String surname,
                             @QueryParam("email") String email, @QueryParam("login") String login,
                             @QueryParam("page") Integer page) {
        String jpql = "FROM SruUser u WHERE 1=1 ";
        if (StringUtils.isNotBlank(name)) {
            jpql += "AND LOWER(u.firstName) LIKE :name ";
        }
        if (StringUtils.isNotBlank(surname)) {
            jpql += "AND LOWER(u.lastName) LIKE :surname ";
        }
        if (StringUtils.isNotBlank(email)) {
            jpql += "AND LOWER(u.email) LIKE :email ";
        }
        if (StringUtils.isNotBlank(login)) {
            jpql += "AND LOWER(u.login) LIKE :login ";
        }
        TypedQuery<SruUser> query = entityManager.createQuery("SELECT u " + jpql + "ORDER BY u.lastName, u.firstName, u.uuid", SruUser.class);
        TypedQuery<Long> queryCount = entityManager.createQuery("SELECT COUNT(u) " + jpql, Long.class);
        Query[] queries = {query, queryCount};
        for (Query q : queries) {
            if (StringUtils.isNotBlank(name)) {
                q.setParameter("name", name.toLowerCase() + "%");
            }
            if (StringUtils.isNotBlank(surname)) {
                q.setParameter("surname", surname.toLowerCase() + "%");
            }
            if (StringUtils.isNotBlank(email)) {
                q.setParameter("email", email.toLowerCase() + "%");
            }
            if (StringUtils.isNotBlank(login)) {
                q.setParameter("login", login.toLowerCase() + "%");
            }
        }
        if (page != null) {
            query.setFirstResult(page * itemsPerPage);
            query.setMaxResults(itemsPerPage);
        }
        List<SruUser> users = query.getResultList();
        Long count = queryCount.getSingleResult();
        List<ProfileResponseDTO> responseDTOList = users.stream().map(this::convertUserToDTO).collect(Collectors.toList());
        return new Object() {
            public final List<ProfileResponseDTO> users = responseDTOList;
            public final int pages = (int) Math.ceil((double) count / itemsPerPage);
        };
    }

    @POST
    @Path("/user/auth")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(LoginRequestDTO log) {
        if (log.getPassword() == null || log.getPassword().length() < minPasswordLength) {
            throw new ServiceException(ServiceException.ExceptionType.PASSWORD_LENGTH);
        } else if (log.getLogin() == null || log.getLogin().length() < 1) {
            throw new ServiceException(ServiceException.ExceptionType.LOGIN_LENGTH);
        }
        SruUser sruUser = findUserByLoginAndPassword(log.getLogin(), log.getPassword());
        if (sruUser != null) {
            NewCookie cookie = cookieProvider.createSessionCookie(sruUser);
            return Response.ok().entity(new Object()).cookie(cookie).build();
        } else {
            throw new ServiceException(ServiceException.ExceptionType.BAD_PASSWORD);
        }
    }

    @GET
    @Path("/user/auth")
    @Secured(SruRole.USER)
    public Object login(@Context SecurityContext securityContext) {
        SruUser sruUser = getSruUser(securityContext);
        if (sruUser != null) {
            return new Object();
        } else {
            throw new ServiceException(ServiceException.ExceptionType.BAD_PASSWORD);
        }
    }

    @DELETE
    @Path("/user/auth")
    @Secured(SruRole.USER)
    public Response logout(@Context SecurityContext securityContext) {
        SruUser sruUser = getSruUser(securityContext);
        if (sruUser != null) {
            NewCookie cookie = cookieProvider.createLogoutCookie(sruUser);
            return Response.ok().entity(new Object()).cookie(cookie).build();
        } else {
            throw new ServiceException(ServiceException.ExceptionType.BAD_PASSWORD);
        }
    }

    public ProfileResponseDTO convertUserToDTO(SruUser sruUser) {
        ProfileResponseDTO profile = new ProfileResponseDTO();
        fillSruUserResponseDTO(profile, sruUser);
        return profile;
    }

    public void fillSruUserResponseDTO(ProfileResponseDTO response, SruUser sruUser) {
        response.setUuid(sruUser.getUuid());
        response.setUserType(sruUser.getUserType().toString());
        response.setEmail(sruUser.getEmail());
        response.setFirstName(sruUser.getFirstName());
        response.setLastName(sruUser.getLastName());
        response.setStudentId(sruUser.getStudentId());
    }

    private void saveUserToHistory(SruUser sruUser, SruUser modifiedBy) {
        SruUserHistory history = new SruUserHistory();
        history.setSruUser(sruUser);
        history.setTargetUuid(sruUser.getUuid());
        history.setModifiedBy(modifiedBy);
        entityManager.persist(history);
    }

    private SruUser findUserByLogin(String login) {
        String jpql = "SELECT u " +
                "FROM SruUser u " +
                "WHERE u.login = :login";
        TypedQuery<SruUser> q = entityManager.createQuery(jpql, SruUser.class);
        q.setParameter("login", login);
        return getElementFromQuery(q);
    }

    public SruUser findUserByUuid(String uuid) {
        String jpql = "SELECT u " +
                "FROM SruUser u " +
                "WHERE u.uuid = :uuid";
        TypedQuery<SruUser> q = entityManager.createQuery(jpql, SruUser.class);
        q.setParameter("uuid", uuid);
        return getElementFromQuery(q);
    }

    private SruUser findUserByLoginAndPassword(String login, String password) {
        SruUser sruUser = findUserByLogin(login);
        if (sruUser != null && sruUser.getbPassword() != null && sruUser.getbPassword().equalsHashes(password)) {
            return sruUser;
        }
        return null;
    }
}