package pl.gda.pg.ds.sru.entity.base;

import pl.gda.pg.ds.sru.entity.SruUser;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import java.util.Date;

@MappedSuperclass
public class HistoricalDataEntity extends BasicEntity {
    private Date validFrom = new Date();
    @OneToOne
    private SruUser modifiedBy;
    private String targetUuid;

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public SruUser getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(SruUser modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getTargetUuid() {
        return targetUuid;
    }

    public void setTargetUuid(String targetUuid) {
        this.targetUuid = targetUuid;
    }
}
