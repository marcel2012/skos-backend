package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.BasicEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Embeddable
public class Device extends BasicEntity implements Serializable {

    @Column(nullable = false)
    private String hostName;
    private Date lastSeen;
    @Embedded
    private MacAddress macAddress;
    private boolean deactivateInactive = true;
    @ManyToOne
    @Column(nullable = false)
    private SruUser sruUser;
    @ManyToOne
    @Column(nullable = false)
    private DeviceType deviceType;
    @OneToMany(mappedBy = "device")
    private Set<DeviceDHCPLease> dhcpLeases;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public MacAddress getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(MacAddress macAddress) {
        this.macAddress = macAddress;
    }

    public Date getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(Date lastSeen) {
        this.lastSeen = lastSeen;
    }

    public boolean isDeactivateInactive() {
        return deactivateInactive;
    }

    public void setDeactivateInactive(boolean deactivateInactive) {
        this.deactivateInactive = deactivateInactive;
    }

    public SruUser getSruUser() {
        return sruUser;
    }

    public void setSruUser(SruUser sruUser) {
        this.sruUser = sruUser;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public Set<DeviceDHCPLease> getDhcpLeases() {
        return dhcpLeases;
    }

    public void setDhcpLeases(Set<DeviceDHCPLease> dhcpLeases) {
        this.dhcpLeases = dhcpLeases;
    }
}
