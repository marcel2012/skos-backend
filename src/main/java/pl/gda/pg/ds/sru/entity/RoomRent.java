package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.LeaseDataEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class RoomRent extends LeaseDataEntity {
    @ManyToOne
    @Column(nullable = false)
    private Room room;
    @ManyToOne
    @Column(nullable = false)
    private SruUser sruUser;

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public SruUser getSruUser() {
        return sruUser;
    }

    public void setSruUser(SruUser sruUser) {
        this.sruUser = sruUser;
    }
}
