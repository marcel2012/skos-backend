package pl.gda.pg.ds.sru.dto;

import java.util.Date;

public class SruUserHistoryResponseDTO extends ProfileResponseDTO {
    private Date validFrom;
    private ProfileResponseDTO modifiedBy;

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public ProfileResponseDTO getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(ProfileResponseDTO modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
