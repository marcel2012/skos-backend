package pl.gda.pg.ds.sru.service;

import pl.gda.pg.ds.sru.dto.DeviceRegistrationRequestDTO;
import pl.gda.pg.ds.sru.dto.DeviceResponseDTO;
import pl.gda.pg.ds.sru.entity.*;
import pl.gda.pg.ds.sru.exception.ServiceException;
import pl.gda.pg.ds.sru.security.Secured;
import pl.gda.pg.ds.sru.security.SruRole;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static pl.gda.pg.ds.sru.exception.ServiceException.ExceptionType.BAD_UUID;
import static pl.gda.pg.ds.sru.service.helper.QueryResult.getElementFromQuery;

@Stateless
public class DeviceService {
    private static final int minHostNameLength = 6;
    private static final int maxHostNameLength = 20;
    private static final Pattern hostNamePattern = Pattern.compile("^[a-zA-Z]*$");
    private static final Integer itemsPerPage = 2;

    @Inject
    private UserService userService;
    @Inject
    private DHCPService dhcpService;
    @PersistenceContext
    private EntityManager entityManager;

    public String filterMacAddress(String address) {
        address = address.toLowerCase();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < address.length(); i++) {
            char c = address.charAt(i);
            if (Character.isDigit(c) || (c >= 'a' && c <= 'f')) {
                stringBuilder.append(c);
            }
        }
        return stringBuilder.toString();
    }

    public boolean isMacAddressValid(String address) {
        return filterMacAddress(address).length() == 2 * 6;
    }

    public boolean isHostNameValid(String hostName) {
        return hostNamePattern.matcher(hostName).find();
    }


    @GET
    @Path("/user/me/device/{uuid}")
    @Secured({SruRole.USER})
    public DeviceResponseDTO getMyDevice(@Context SecurityContext securityContext, @PathParam("uuid") String uuid) {
        SruUser sruUser = userService.getSruUser(securityContext);
        return getDevice(uuid, sruUser);
    }

    @GET
    @Path("/user/{userUuid}/device/{uuid}")
    @Secured({SruRole.ADMIN})
    public DeviceResponseDTO getUserDevice(@PathParam("uuid") String uuid, @PathParam("userUuid") String userUuid) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        return getDevice(uuid, sruUser);
    }

    private DeviceResponseDTO getDevice(String uuid, SruUser sruUser) {
        Device device = findDeviceByUuidAndUser(uuid, sruUser);
        if (device != null) {
            return convertDeviceToDTO(device);
        }
        throw new ServiceException(BAD_UUID);
    }

    @GET
    @Path("/user/me/device")
    @Secured({SruRole.USER})
    public List<DeviceResponseDTO> getMyDevices(@Context SecurityContext securityContext) {
        SruUser sruUser = userService.getSruUser(securityContext);
        return getDevices(sruUser);
    }

    @GET
    @Path("/user/{userUuid}/device")
    @Secured({SruRole.ADMIN})
    public List<DeviceResponseDTO> getUserDevices(@PathParam("userUuid") String userUuid) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        return getDevices(sruUser);
    }

    private List<DeviceResponseDTO> getDevices(SruUser sruUser) {
        String jpql = "SELECT d " +
                "FROM Device d " +
                "WHERE d.sruUser = :sruUser " +
                "ORDER BY d.uuid";
        TypedQuery<Device> q = entityManager.createQuery(jpql, Device.class);
        q.setParameter("sruUser", sruUser);
        List<Device> devices = q.getResultList();
        return devices.stream().map(this::convertDeviceToDTO).collect(Collectors.toList());
    }

    @PUT
    @Path("/user/me/device/")
    @Secured({SruRole.USER})
    @Consumes(MediaType.APPLICATION_JSON)
    public Object registerMyDevice(DeviceRegistrationRequestDTO reg, @Context SecurityContext securityContext) {
        SruUser sruUser = userService.getSruUser(securityContext);
        return registerDevice(reg, sruUser);
    }

    @PUT
    @Path("/user/{userUuid}/device/")
    @Secured({SruRole.ADMIN})
    @Consumes(MediaType.APPLICATION_JSON)
    public Object registerUserDevice(DeviceRegistrationRequestDTO reg, @PathParam("userUuid") String userUuid) {
        SruUser sruUser = userService.findUserByUuid(userUuid);
        return registerDevice(reg, sruUser);
    }

    private Object registerDevice(DeviceRegistrationRequestDTO reg, SruUser sruUser) {
        if (reg.getHostName() == null || reg.getHostName().length() < minHostNameLength || reg.getHostName().length() > maxHostNameLength) {
            throw new ServiceException(ServiceException.ExceptionType.HOST_NAME_LENGTH);
        } else if (!isHostNameValid(reg.getHostName())) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_HOST_NAME);
        } else if (findDeviceByHostNameExceptUuid(reg.getHostName(), reg.getUuid()) != null) {
            throw new ServiceException(ServiceException.ExceptionType.HOST_NAME_UNIQUE);
        } else if (reg.getMacAddress() == null) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_MAC_ADDRESS);
        } else if (reg.getDeviceType() == null) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_DEVICE_TYPE);
        }
        DeviceType deviceType = findDeviceTypeByCode(reg.getDeviceType());
        if (deviceType == null) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_DEVICE_TYPE);
        }
        String filteredMacAddress = filterMacAddress(reg.getMacAddress());
        if (!isMacAddressValid(filteredMacAddress)) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_MAC_ADDRESS);
        }
        if (findActiveDeviceByMacExceptUuid(filteredMacAddress, reg.getUuid()) != null) {
            throw new ServiceException(ServiceException.ExceptionType.MAC_ADDRESS_UNIQUE);
        }
        Device device;
        if (reg.getUuid() != null) {
            device = findDeviceByUuidAndUser(reg.getUuid(), sruUser);
            if (device == null) {
                throw new ServiceException(ServiceException.ExceptionType.BAD_UUID);
            }
        } else {
            device = new Device();
            device.setSruUser(sruUser);
        }
        device.setHostName(reg.getHostName());
        device.setMacAddress(new MacAddress(filteredMacAddress));
        device.setDeviceType(deviceType);

        entityManager.persist(device);
        saveDeviceToHistory(device, sruUser);
        return new Object() {
            final String uuid = device.getUuid();
        };
    }

    @GET
    @Path("/device/search")
    @Secured({SruRole.ADMIN})
    public Object getProfile(@QueryParam("hostName") String hostName, @QueryParam("macAddress") String macAddress,
                             @QueryParam("page") Integer page) {
        String jpql = "FROM Device d WHERE 1=1 ";
        if (hostName != null) {
            jpql += "AND LOWER(d.hostName) LIKE :hostName ";
        }
        if (macAddress != null) {
            jpql += "AND LOWER(d.macAddress.address) LIKE :macAddress ";
        }
        TypedQuery<Device> query = entityManager.createQuery("SELECT d " + jpql + "ORDER BY d.hostName, d.uuid", Device.class);
        TypedQuery<Long> queryCount = entityManager.createQuery("SELECT COUNT(d) " + jpql, Long.class);
        Query[] queries = {query, queryCount};
        for (Query q : queries) {
            if (hostName != null) {
                q.setParameter("hostName", hostName.toLowerCase() + "%");
            }
            if (macAddress != null) {
                String filteredMacAddress = filterMacAddress(macAddress);
                q.setParameter("macAddress", filteredMacAddress.toLowerCase() + "%");
            }
        }
        if (page != null) {
            query.setFirstResult(page * itemsPerPage);
            query.setMaxResults(itemsPerPage);
        }
        List<Device> devices = query.getResultList();
        Long count = queryCount.getSingleResult();
        List<DeviceResponseDTO> responseDTOList = devices.stream().map(this::convertDeviceToDTO).collect(Collectors.toList());
        return new Object() {
            public final List<DeviceResponseDTO> devices = responseDTOList;
            public final int pages = (int) Math.ceil((double) count / itemsPerPage);
        };
    }

    private void saveDeviceToHistory(Device device, SruUser modifiedBy) {
        DeviceHistory history = new DeviceHistory();
        history.setDevice(device);
        history.setTargetUuid(device.getUuid());
        history.setModifiedBy(modifiedBy);
        entityManager.persist(history);
    }

    private DeviceResponseDTO convertDeviceToDTO(Device device) {
        DeviceResponseDTO response = new DeviceResponseDTO();
        fillDeviceResponseDTO(response, device);
        response.setUserUuid(device.getSruUser().getUuid());
        return response;
    }

    public void fillDeviceResponseDTO(DeviceResponseDTO response, Device device) {
        response.setUuid(device.getUuid());
        response.setHostName(device.getHostName());
        response.setMacAddress(device.getMacAddress().toString());
        response.setDeviceType(device.getDeviceType().getCode());
    }

    private Device findDeviceByHostNameExceptUuid(String hostName, String uuid) {
        String jpql = "SELECT d " +
                "FROM Device d " +
                "WHERE d.hostName = :hostName " +
                "  AND d.uuid <> :uuid";
        TypedQuery<Device> q = entityManager.createQuery(jpql, Device.class);
        q.setParameter("hostName", hostName);
        q.setParameter("uuid", uuid);
        return getElementFromQuery(q);
    }

    private Device findActiveDeviceByMacExceptUuid(String address, String uuid) {
        String jpql = "SELECT d " +
                "FROM Device d " +
                "WHERE d.macAddress.address = :address " +
                "  AND d.uuid <> :uuid " +
                "  AND EXISTS(SELECT dhcp " +
                "             FROM DeviceDHCPLease dhcp " +
                "             WHERE dhcp.device = d " +
                "               AND dhcp.validFrom <= CURRENT_TIMESTAMP " +
                "               AND (dhcp.validTo IS NULL OR dhcp.validTo >= CURRENT_TIMESTAMP))";
        TypedQuery<Device> q = entityManager.createQuery(jpql, Device.class);
        q.setParameter("address", address);
        q.setParameter("uuid", uuid);
        return getElementFromQuery(q);
    }

    public Device findDeviceByUuidAndUser(String uuid, SruUser sruUser) {
        String jpql = "SELECT d " +
                "FROM Device d " +
                "WHERE d.uuid = :uuid " +
                "  AND d.sruUser = :sruUser";
        TypedQuery<Device> q = entityManager.createQuery(jpql, Device.class);
        q.setParameter("uuid", uuid);
        q.setParameter("sruUser", sruUser);
        return getElementFromQuery(q);
    }

    public DeviceType findDeviceTypeByCode(String code) {
        String jpql = "SELECT d " +
                "FROM DeviceType d " +
                "WHERE d.code = :code";
        TypedQuery<DeviceType> q = entityManager.createQuery(jpql, DeviceType.class);
        q.setParameter("code", code);
        return getElementFromQuery(q);
    }

    public Device findDeviceByUuid(String uuid) {
        String jpql = "SELECT d " +
                "FROM Device d " +
                "WHERE d.uuid = :uuid";
        TypedQuery<Device> q = entityManager.createQuery(jpql, Device.class);
        q.setParameter("uuid", uuid);
        return getElementFromQuery(q);
    }
}
