package pl.gda.pg.ds.sru.security;

public enum SruRole {
    USER, ADMIN, OFFICE
}
