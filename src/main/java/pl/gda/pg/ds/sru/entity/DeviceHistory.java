package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.HistoricalDataEntity;

import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
public class DeviceHistory extends HistoricalDataEntity {

    @Embedded
    private Device device;

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}
