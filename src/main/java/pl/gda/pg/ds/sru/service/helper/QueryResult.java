package pl.gda.pg.ds.sru.service.helper;

import javax.persistence.TypedQuery;
import java.util.List;

public class QueryResult {

    public static <T> T getElementFromQuery(TypedQuery<T> query) {
        List<T> list = query.getResultList();
        if (list != null && list.size() > 0) {
            if (list.size() > 1) {
                throw new RuntimeException("Multiple elements; expected one");
            }
            return list.get(0);
        }
        return null;
    }
}
