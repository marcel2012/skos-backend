package pl.gda.pg.ds.sru.exception;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ServiceExceptionMapper implements ExceptionMapper<ServiceException> {
    private static final Logger logger = LogManager.getLogger(ServiceExceptionMapper.class);

    @Override
    public Response toResponse(ServiceException serviceException) {
        logger.error(serviceException);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new Object() {
                    public final String error = serviceException.getExceptionType().toString();
                })
                .build();
    }
}