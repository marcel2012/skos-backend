package pl.gda.pg.ds.sru.entity;

import javax.persistence.Embeddable;

@Embeddable
public class MacAddress {
    private String address;

    public MacAddress() {
    }

    public MacAddress(String address) {
        setAddress(address);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String toString() {
        return String.format("%s:%s:%s:%s:%s:%s",
                address.substring(0, 2),
                address.substring(2, 4),
                address.substring(4, 6),
                address.substring(6, 8),
                address.substring(8, 10),
                address.substring(10, 12)
        );
    }
}
