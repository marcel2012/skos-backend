package pl.gda.pg.ds.sru.security;

import pl.gda.pg.ds.sru.entity.Session;
import pl.gda.pg.ds.sru.entity.helper.UserType;

import javax.annotation.Priority;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.security.Principal;
import java.util.*;

import static pl.gda.pg.ds.sru.service.helper.QueryResult.getElementFromQuery;

@Provider
@Secured
@Priority(Priorities.AUTHENTICATION)
public class Authentication implements ContainerRequestFilter {
    public static final int sessionExpirationMinutes = 15;
    private static final Map<UserType, List<SruRole>> userRoleMap = new HashMap<>();
    private static final CookieProvider cookieProvider = new CookieProvider();

    static {
        userRoleMap.put(UserType.DEFAULT, Collections.singletonList(SruRole.USER));
        userRoleMap.put(UserType.STUDENT, Collections.singletonList(SruRole.USER));
        userRoleMap.put(UserType.ADMINISTRATOR, Arrays.asList(SruRole.USER, SruRole.ADMIN));
        userRoleMap.put(UserType.DORMITORY_OFFICE, Arrays.asList(SruRole.USER, SruRole.OFFICE));
        userRoleMap.put(UserType.RECEPTION, Arrays.asList(SruRole.USER, SruRole.OFFICE));
    }

    private final EntityManagerFactory entityManagerFactory;

    public Authentication() {
        entityManagerFactory = Persistence.createEntityManagerFactory("sru-jpa");
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        try {
            Cookie cookie = cookieProvider.getCookie(requestContext);
            if (cookie == null) {
                throw new SecurityException();
            }

            String token = cookie.getValue();
            validateToken(token, requestContext);
        } catch (SecurityException e) {
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        } catch (Exception e) {
            e.printStackTrace();
            requestContext.abortWith(Response.status(Response.Status.INTERNAL_SERVER_ERROR).build());
        }
    }

    private void validateToken(String token, ContainerRequestContext requestContext) throws SecurityException {
        Session session = findSessionByToken(token);
        if (session != null) {
            session.setRecentlyActive(new Date());
            final String uuid = session.getSruUser().getUuid();
            final UserType userType = session.getSruUser().getUserType();
            requestContext.setSecurityContext(new SecurityContext() {

                @Override
                public Principal getUserPrincipal() {
                    return () -> uuid;
                }

                @Override
                public boolean isUserInRole(String role) {
                    return userRoleMap.get(userType).contains(SruRole.valueOf(role));
                }

                @Override
                public boolean isSecure() {
                    return false;
                }

                @Override
                public String getAuthenticationScheme() {
                    return null;
                }
            });
        } else {
            throw new SecurityException();
        }
    }

    private Session findSessionByToken(String token) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, -sessionExpirationMinutes);
        Date startDate = calendar.getTime();

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        TypedQuery<Session> q = entityManager.createQuery("SELECT s FROM Session s WHERE s.token = :token AND s.recentlyActive >= :startDate", Session.class);
        q.setParameter("token", token);
        q.setParameter("startDate", startDate);
        return getElementFromQuery(q);
    }
}