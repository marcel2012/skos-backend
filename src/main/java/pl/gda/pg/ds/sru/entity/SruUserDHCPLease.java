package pl.gda.pg.ds.sru.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class SruUserDHCPLease extends DHCPLease {

    @ManyToOne
    private SruUser sruUser;

    public SruUser getSruUser() {
        return sruUser;
    }

    public void setSruUser(SruUser sruUser) {
        this.sruUser = sruUser;
    }
}
