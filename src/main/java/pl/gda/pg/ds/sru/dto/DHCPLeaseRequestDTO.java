package pl.gda.pg.ds.sru.dto;

public class DHCPLeaseRequestDTO {
    private String ipAddressUuid;

    public String getIpAddressUuid() {
        return ipAddressUuid;
    }

    public void setIpAddressUuid(String ipAddressUuid) {
        this.ipAddressUuid = ipAddressUuid;
    }
}
