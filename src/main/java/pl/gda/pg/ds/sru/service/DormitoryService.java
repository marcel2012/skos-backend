package pl.gda.pg.ds.sru.service;

import pl.gda.pg.ds.sru.dto.*;
import pl.gda.pg.ds.sru.entity.Dormitory;
import pl.gda.pg.ds.sru.entity.Room;
import pl.gda.pg.ds.sru.entity.RoomRent;
import pl.gda.pg.ds.sru.exception.ServiceException;
import pl.gda.pg.ds.sru.security.Secured;
import pl.gda.pg.ds.sru.security.SruRole;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static pl.gda.pg.ds.sru.exception.ServiceException.ExceptionType.BAD_UUID;
import static pl.gda.pg.ds.sru.service.helper.QueryResult.getElementFromQuery;

@Path("/dormitory")
@Stateless
@Produces(MediaType.APPLICATION_JSON)
@Secured({SruRole.ADMIN, SruRole.OFFICE})
public class DormitoryService {

    private static final int minNameLength = 3;
    private static final int maxNameLength = 10;
    private static final Pattern namePattern = Pattern.compile("^[0-9\\p{L}]*$");
    private static final Integer itemsPerPage = 10;

    @PersistenceContext
    private EntityManager entityManager;
    @Inject
    private RoomRentService roomRentService;

    private boolean isNameValid(String name) {
        return namePattern.matcher(name).find();
    }

    private boolean isPlacesValid(Integer places) {
        return places != null && (places >= 0 && places <= 3);
    }

    @GET
    @Path("/")
    public List<DormitoryResponseDTO> getDormitories() {
        TypedQuery<Dormitory> q = entityManager.createQuery("SELECT d FROM Dormitory d ORDER BY d.name", Dormitory.class);
        List<Dormitory> dormitories = q.getResultList();
        return dormitories.stream().map(this::convertDormitoryToDTO).collect(Collectors.toList());
    }

    @GET
    @Path("/{uuid}")
    public DormitoryResponseDTO getDormitory(@PathParam("uuid") String uuid) {
        Dormitory dormitory = findDormitoryByUuid(uuid);
        if (dormitory != null) {
            return convertDormitoryToDTO(dormitory);
        }
        throw new ServiceException(BAD_UUID);
    }

    @GET
    @Path("/{uuid}/room/inhabitant")
    public List<InhabitantRoomResponseDTO> getDormitoryRoomsInhabitants(@PathParam("uuid") String uuid) {
        Dormitory dormitory = findDormitoryByUuid(uuid);
        if (dormitory != null) {
            Set<Room> rooms = dormitory.getRooms();
            return rooms
                    .stream()
                    .sorted()
                    .map(this::convertRoomToInhabitantDTO)
                    .collect(Collectors.toList());
        }
        throw new ServiceException(BAD_UUID);
    }

    @GET
    @Path("/{uuid}/room")
    public List<RoomResponseDTO> getDormitoryRooms(@PathParam("uuid") String uuid) {
        Dormitory dormitory = findDormitoryByUuid(uuid);
        if (dormitory != null) {
            Set<Room> rooms = dormitory.getRooms();
            return rooms
                    .stream()
                    .sorted()
                    .map(this::convertRoomToDTO)
                    .collect(Collectors.toList());
        }
        throw new ServiceException(BAD_UUID);
    }

    @GET
    @Path("/{uuid}/room/search")
    public Object getDormitoryRooms(@PathParam("uuid") String uuid, @QueryParam("name") String name, @QueryParam("page") Integer page) {
        String jpql = "FROM Room r WHERE 1=1 ";
        if (uuid != null) {
            jpql += "AND r.dormitory.uuid = :uuid ";
        }
        if (name != null) {
            jpql += "AND LOWER(r.name) LIKE :name ";
        }
        TypedQuery<Room> query = entityManager.createQuery("SELECT r " + jpql + "ORDER BY r.name, r.uuid", Room.class);
        TypedQuery<Long> queryCount = entityManager.createQuery("SELECT COUNT(r) " + jpql, Long.class);
        Query[] queries = {query, queryCount};
        for (Query q : queries) {
            if (uuid != null) {
                q.setParameter("uuid", uuid);
            }
            if (name != null) {
                q.setParameter("name", name.toLowerCase() + "%");
            }
        }
        if (page != null) {
            query.setFirstResult(page * itemsPerPage);
            query.setMaxResults(itemsPerPage);
        }
        List<Room> devices = query.getResultList();
        Long count = queryCount.getSingleResult();
        List<RoomResponseDTO> responseDTOList = devices.stream().map(this::convertRoomToDTO).collect(Collectors.toList());
        return new Object() {
            public final List<RoomResponseDTO> rooms = responseDTOList;
            public final int pages = (int) Math.ceil((double) count / itemsPerPage);
        };
    }

    @GET
    @Path("/{uuid}/room/{roomUuid}")
    public RoomResponseDTO getDormitoryRoom(@PathParam("uuid") String uuid, @PathParam("roomUuid") String roomUuid) {
        Room room = findRoomByUuidAndDormitory(roomUuid, uuid);
        if (room != null) {
            return convertRoomToDTO(room);
        }
        throw new ServiceException(BAD_UUID);
    }

    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Object createDormitory(DormitoryRequestDTO reg) {
        if (reg.getName() == null || reg.getName().length() < minNameLength || reg.getName().length() > maxNameLength) {
            throw new ServiceException(ServiceException.ExceptionType.DORMITORY_NAME_LENGTH);
        } else if (!isNameValid(reg.getName())) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_DORMITORY_NAME);
        } else if (findDormitoryByNameExceptUuid(reg.getName(), reg.getUuid()) != null) {
            throw new ServiceException(ServiceException.ExceptionType.DORMITORY_NAME_UNIQUE);
        }
        Dormitory dormitory;
        if (reg.getUuid() != null) {
            dormitory = findDormitoryByUuid(reg.getUuid());
            if (dormitory == null) {
                throw new ServiceException(BAD_UUID);
            }
        } else {
            dormitory = new Dormitory();
        }
        dormitory.setName(reg.getName());

        entityManager.persist(dormitory);
        return new Object() {
            public final String uuid = dormitory.getUuid();
        };
    }

    @PUT
    @Path("/{uuid}/room")
    @Consumes(MediaType.APPLICATION_JSON)
    public Object createRoom(RoomRequestDTO reg, @PathParam("uuid") String dormitoryUuid) {
        Dormitory dormitory = findDormitoryByUuid(dormitoryUuid);
        if (dormitory == null) {
            throw new ServiceException(BAD_UUID);
        } else if (reg.getName() == null || reg.getName().length() < minNameLength || reg.getName().length() > maxNameLength) {
            throw new ServiceException(ServiceException.ExceptionType.ROOM_NAME_LENGTH);
        } else if (!isNameValid(reg.getName())) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_ROOM_NAME);
        } else if (!isPlacesValid(reg.getPlaces())) {
            throw new ServiceException(ServiceException.ExceptionType.BAD_ROOM_PLACES);
        } else if (findRoomByNameAndDormitoryExceptUuid(reg.getName(), dormitoryUuid, reg.getUuid()) != null) {
            throw new ServiceException(BAD_UUID);
        }
        Room room;
        if (reg.getUuid() != null) {
            room = findRoomByUuidAndDormitory(reg.getUuid(), dormitoryUuid);
            if (room == null) {
                throw new ServiceException(BAD_UUID);
            }
        } else {
            room = new Room();
            room.setDormitory(dormitory);
        }
        room.setName(reg.getName());
        room.setPlaces(reg.getPlaces());

        entityManager.persist(room);
        return new Object() {
            public final String uuid = room.getUuid();
        };
    }

    @GET
    @Path("/{uuid}/room/{roomUuid}/inhabitant")
    public InhabitantRoomResponseDTO getDormitoryRooms(@PathParam("uuid") String dormitoryUuid, @PathParam("roomUuid") String roomUuid) {
        Room room = findRoomByUuidAndDormitory(roomUuid, dormitoryUuid);
        if (room != null) {
            return convertRoomToInhabitantDTO(room);
        }
        throw new ServiceException(BAD_UUID);
    }

    public DormitoryResponseDTO convertDormitoryToDTO(Dormitory dormitory) {
        DormitoryResponseDTO response = new DormitoryResponseDTO();
        response.setUuid(dormitory.getUuid());
        response.setName(dormitory.getName());
        return response;
    }

    private InhabitantRoomResponseDTO convertRoomToInhabitantDTO(Room room) {
        InhabitantRoomResponseDTO response = new InhabitantRoomResponseDTO();
        response.setUuid(room.getUuid());
        response.setName(room.getName());
        response.setPlaces(room.getPlaces());
        response.setUsers(roomRentService.getActiveRentsForRoom(room).stream().map(RoomRent::getSruUser).sorted().map(user -> {
            ProfileResponseDTO res = new ProfileResponseDTO();
            res.setUuid(user.getUuid());
            res.setFirstName(user.getFirstName());
            res.setLastName(user.getLastName());
            return res;
        }).collect(Collectors.toList()));
        return response;
    }

    public RoomResponseDTO convertRoomToDTO(Room room) {
        RoomResponseDTO response = new RoomResponseDTO();
        response.setUuid(room.getUuid());
        response.setName(room.getName());
        response.setPlaces(room.getPlaces());
        return response;
    }

    private Dormitory findDormitoryByNameExceptUuid(String name, String uuid) {
        TypedQuery<Dormitory> q = entityManager.createQuery("SELECT d FROM Dormitory d WHERE d.name = :name AND d.uuid <> :uuid", Dormitory.class);
        q.setParameter("name", name);
        q.setParameter("uuid", uuid);
        return getElementFromQuery(q);
    }

    private Room findRoomByNameAndDormitoryExceptUuid(String name, String dormitoryUuid, String roomUuid) {
        TypedQuery<Room> q = entityManager.createQuery("SELECT r FROM Room r WHERE r.name = :name AND r.uuid <> :roomUuid AND r.dormitory.uuid = :dormitoryUuid", Room.class);
        q.setParameter("name", name);
        q.setParameter("dormitoryUuid", dormitoryUuid);
        q.setParameter("roomUuid", roomUuid);
        return getElementFromQuery(q);
    }

    private Dormitory findDormitoryByUuid(String uuid) {
        TypedQuery<Dormitory> q = entityManager.createQuery("SELECT d FROM Dormitory d WHERE d.uuid = :uuid", Dormitory.class);
        q.setParameter("uuid", uuid);
        return getElementFromQuery(q);
    }

    public Room findRoomByUuidAndDormitory(String roomUuid, String dormitoryUuid) {
        TypedQuery<Room> q = entityManager.createQuery("SELECT r FROM Room r WHERE r.uuid = :roomUuid AND r.dormitory.uuid = :dormitoryUuid", Room.class);
        q.setParameter("roomUuid", roomUuid);
        q.setParameter("dormitoryUuid", dormitoryUuid);
        return getElementFromQuery(q);
    }
}
