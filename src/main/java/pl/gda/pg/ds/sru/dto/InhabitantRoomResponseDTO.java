package pl.gda.pg.ds.sru.dto;

import java.util.List;

public class InhabitantRoomResponseDTO extends RoomResponseDTO {
    private List<ProfileResponseDTO> users;

    public List<ProfileResponseDTO> getUsers() {
        return users;
    }

    public void setUsers(List<ProfileResponseDTO> users) {
        this.users = users;
    }
}
