package pl.gda.pg.ds.sru.entity;

import pl.gda.pg.ds.sru.entity.base.HistoricalDataEntity;

import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
public class SruUserHistory extends HistoricalDataEntity {

    @Embedded
    private SruUser sruUser;

    public SruUser getSruUser() {
        return sruUser;
    }

    public void setSruUser(SruUser sruUser) {
        this.sruUser = sruUser;
    }
}
