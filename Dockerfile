FROM maven:latest as maven

WORKDIR /build/

COPY pom.xml .
RUN mvn verify --fail-never

COPY src/ src/
RUN mvn package



FROM tomee:11-jre-8.0.2-plus as tomee

WORKDIR /usr/local/tomee/

RUN useradd tomee
RUN chown -R tomee .
USER tomee

RUN rm -rf webapps/*

COPY --from=maven /build/target/api.war webapps/

# cookie domain
ENV DOMAIN "localhost"

# debugger
ENV CATALINA_OPTS "$CATALINA_OPTS -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005"
EXPOSE 5005

# local database connection
ENV DB_USER "postgres"
ENV DB_PASSWORD "password"
ENV DATABASE_URL "jdbc:postgresql://localhost:5432/postgres"
ENV CATALINA_OPTS "$CATALINA_OPTS -Ddb.password=$DB_PASSWORD -Ddb.username=$DB_USER -Ddb.url=$DATABASE_URL"

# mail server
ENV MAIL_SERVER ""
ENV MAIL_USER ""
ENV MAIL_PASSWORD ""
ENV MAIL_ADDRESS ""
ENV MAIL_LOGGER ""
ENV CATALINA_OPTS "$CATALINA_OPTS -Dmail.server=$MAIL_SERVER -Dmail.user=$MAIL_USER -Dmail.password=$MAIL_PASSWORD -Dmail.address=$MAIL_ADDRESS -Dmail.debug=$MAIL_LOGGER"

# logger
ENV CATALINA_OPTS "$CATALINA_OPTS -Dmail.smtp.starttls.required=true"
