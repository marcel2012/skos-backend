# SRU backend

## Database

`docker run --name sru-postgres -e POSTGRES_PASSWORD=password -d -p 5432:5432 postgres`

## Compilation

`mvn package`

`docker build -t sru-backend .`

## Start-up

`docker run -p 8080:8080 -p 5005:5005 --name sru-backend --net host sru-backend`

## Maven dependency report
[report](https://gitlab.com/marcel2012/skos-backend/-/jobs/artifacts/master/raw/target/dependency-check-report.html?job=verify)
